# README #

Collection of code golf project.

### What is this repository for? ###

* Just for fun

### Projects links ###

* [Speed of letters](https://codegolf.stackexchange.com/questions/145246/the-speed-of-letters)
* [Munge my Password](https://codegolf.stackexchange.com/questions/145767/munge-my-password)
* [Square pyramidal numbers](https://codegolf.stackexchange.com/questions/145518/square-pyramidal-numbers/145659)