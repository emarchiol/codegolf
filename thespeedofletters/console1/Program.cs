﻿using System;

namespace console1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Old.
            //f=>{var s=new char[f.Length+26];for(int i=0;i<f.Length;i++)s[f[i]+i-65]=f[i];return s;}
            System.Func<string, char[]> func = f=>{int i=0,l=f.Length;var s=new char[l+26];for(;i<l;i++)s[f[i]+i-65]=f[i];return s;}
            ;
            //Lan H.
            //n=>{int i=0,l=n.Length;var t=new char[l+26];for(;i<l;)t[i+n[i]-65]=n[i++];return t;}

            string[] cases = new string[] { "BA", "ACE", "HELLOWORLD", "PPCG" };
            foreach (string s in cases) System.Console.WriteLine(func(s));
        }
    }
}
