﻿using System;

namespace MungeMyPassword
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                string[] tests = { "codegolf", "programming", "puzzles", "passwords" };
                Func<string, string> cg = g => { return string.Empty; };
                foreach (string s in tests)
                {
                    Console.WriteLine(s + " : " + Dowork(s.ToCharArray()));
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static string Dowork(char[] pas)
        {
            char[] cases = ("a@b8c(d6e3f#g9h#i1i!k<l1lio0q9s5s$t+v>v<wuw2x%y?").ToCharArray();
            //var casesD = "abcdefghijklmnopqrstuvwxyz";
            var result = string.Empty;
            int i = 0, n = pas.Length;
            int equalsChar = 1;

            // Doubles
            for (i = 0; i < n; i++)
            {
                char currentChar = pas[i];
                // cambia tutte le apparizioni multipli e modifica con le regole solo quel carattere
                char nextChar = pas[(i == n - 1 ? i : i + 1)];

                if (currentChar == Char.ToLower(nextChar) && i != n - 1)
                {
                    equalsChar++;
                }
                else
                {
                    // finito con le doppie per questo carattere
                    int casesIndex = new string(cases).IndexOf(Char.ToLower(currentChar));
                    if (casesIndex >= 0 && casesIndex % 2 < 1)
                    {
                        currentChar = cases[casesIndex + 1];
                        cases[casesIndex] = '_';
                    }

                    if (equalsChar > 1)
                    {
                        result += equalsChar + "" + currentChar;
                    }
                    else
                    {
                        result += currentChar;
                    }

                    if (currentChar == 'u' && casesIndex % 2 < 1) result += 'u';
                    equalsChar = 1;
                }
            }

            return result;
        }

        public static string DoworkCompressed(char[] p)
        {
            var cases = ("a@b8c(d6e3f#g9h#i1i!k<l1lio0q9s5s$t+v>v<wuw2x%y?").ToCharArray();
            var result = string.Empty;
            int i = 0, n = p.Length;
            int equalsChar = 1;

            for (i = 0; i < n; i++)
            {
                char currentChar = p[i];
                char nextChar = p[(i == n - 1 ? i : i + 1)];

                if (currentChar == Char.ToLower(nextChar) && i != n - 1)
                {
                    equalsChar++;
                }
                else
                {
                    int casesIndex = new string(cases).IndexOf(Char.ToLower(currentChar));
                    if (casesIndex >= 0 && casesIndex % 2 < 1)
                    {
                        currentChar = cases[casesIndex + 1];
                        cases[casesIndex] = '_';
                    }

                    if (equalsChar > 1)
                    {
                        result += equalsChar + "" + currentChar;
                    }
                    else
                    {
                        result += currentChar;
                    }

                    if (currentChar == 'u' && casesIndex % 2 < 1) result += 'u';
                    equalsChar = 1;
                }
            }

            return result;
        }
    }
}